function init() {
    var mymap = L.map('myMap',
            {
                center: [4.625770, -74.172777],
                zoom: 16
            });

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {

        maxZoom: 20,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.streets'
    }).addTo(mymap);

    var marker = L.marker([4.625770, -74.172777]).addTo(mymap);

    var polygon = L.polygon([ 
[ 4.623, -74.173],[4.624,-74.178],[4.625,-74.175],[4.628,-74.17]
]).addTo(mymap);

    
    marker.bindPopup("Gran Britalia!!!");
    polygon.bindPopup("El mejor barrio de Bogotá!!!.");


}




